import React, {useEffect, useState } from 'react';

function PresentationForm(props) {
    const [conference, setConference] = useState('');
    const [conferences, setConferences] = useState([]);
    const [presenter_name, setPresenterName] = useState('');
    const [presenter_email, setPresenterEmail] = useState('');
    const [company_name, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
      }
      useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {

        event.preventDefault();
        const data = {};
        data.conference = conference;
        data.presenter_name = presenter_name;
        data.presenter_email = presenter_email;
        data.company_name = company_name;
        data.title = title;
        data.synopsis = synopsis;

        console.log("data", data)

        const presentationUrl = `http://localhost:8000${conference}presentations/`;
        const fetchOptions = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if (presentationResponse.ok) {
            setConference('');
            setPresenterName('');
            setPresenterEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
        }
      }

      const handleChangeConference = (event) => {
        const value = event.target.value;
        setConference(value);
      }

      const handleChangePresenterName = (event) => {
        const value = event.target.value;
        setPresenterName(value);
      }

      const handleChangePresenterEmail = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
      }

      const handleChangeCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value);
      }
      const handleChangeTitle = (event) => {
        const value = event.target.value;
        setTitle(value);
      }
      const handleChangeSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
      }





    return (
<>
  <main>
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value={presenter_name} onChange={handleChangePresenterName}placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={presenter_email} onChange={handleChangePresenterEmail}placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value={company_name} onChange={handleChangeCompanyName}placeholder="Company name" type="text" name="company_name" id="company_name" required className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={title }onChange={handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value={synopsis }onChange={handleChangeSynopsis} className="form-control" id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleChangeConference} name="conference" id="conference" required>
                  <option value={conferences}>Choose a conference</option>
                  {conferences.map(conference => {
                      return (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      )
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
              </form>
          </div>
        </div>
      </div>
    </div>
  </main>

</>

      );
}

export default PresentationForm
