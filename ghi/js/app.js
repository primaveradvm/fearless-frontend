function createCard(name, description, pictureUrl, date, location) {
    return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">${date}</li>
        </ul>
    </div>
    `;
}

function holyShit(badResponse){
    return `
    <div class="alert alert-primary" role="alert">
  ${badResponse}Try again.
</div>

    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    //make a variable "columns", select every matching element
    const columns = document.querySelector(".col") //render into DOM




    try {
        const response = await fetch(url);
        if (response.ok) {
            // what to do if response is bad
            console.log('Error: bad response')
            const badResponse = "holy shit bad response@#$!"
            const html = holyShit(badResponse)
            innerHTML += html;
            console.log(html)


        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    const date = details.conference.starts + "-" + details.conference.ends;
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl,date, location)
                    //18-35 info from API, renders into HTML string

                    console.log(html);
                    // const columnList = document.querySelectorAll(".col")
                    // for (let column of columnList) {
                    //     column.innerHTML += html;
                    // }

                    columns.innerHTML += html; //renders into DOM
                    // columns[0].innerHTML += html; ******FOR LOOP HERE???****************



            }
            }
        }
    } catch (e) {
        //what to do if error is raised
        // console.log('An error occured fetching data from the server')
    }
    // console.log(response);

    // console.log(data);

});
